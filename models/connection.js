const Sequelize = require('sequelize');
const connectionDb = require('../connectionDb');

const Connection = connectionDb.define('usuarios', {
   user: {
       type: Sequelize.STRING
   },
   email: {
       type: Sequelize.STRING
   },
   password: {
       type: Sequelize.STRING
   }
});

//Connection.sync({force: true});

module.exports = Connection;