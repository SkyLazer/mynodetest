const express = require('express');
const app = express();
const handlebars = require('express-handlebars');
const bodyParser = require('body-parser');
const Connection = require(__dirname+'/models/connection');

//config banco de dados -
const connectionDb = require(__dirname+'/connectionDb');

//config handlebars - 
app.engine('handlebars', handlebars({defaultLayout: "main"}));
app.set('view engine', 'handlebars');

//config bodyParser
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('/', function(req,res){
    res.render(__dirname+'/views/layouts/paginaInicial');
});

app.post('/newUser', function(req,res){
    Connection.create({
        user: req.body.user,
        email: req.body.email,
        password: req.body.password
    }).then(function(){
        res.send("Cadastrado com Sucesso");
    }).catch(function(error){
        res.send("Erro ao se cadastrar" +error);
    });
});

app.get('/cadastro', function(req,res){
    res.render(__dirname+'/views/layouts/cadastro');
});

app.get('/entrar', function(req,res){
    res.render(__dirname+'/views/layouts/entrar');
});

app.post('/log', function(req,res){
    Connection.findOne({where: {email: req.body.email}, where: {password: req.body.password}}).then(function(user, error){
        if(user == null){
            res.send("Falaha ao se conectar "+ error);
        } else {
            res.render(__dirname+'/views/layouts/userPage', {user: user});
        }
    });
});

app.listen(8081, ()=>{
    console.log('Conectado com sucesso');
});
