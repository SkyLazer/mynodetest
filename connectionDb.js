const Sequelize = require('sequelize');
const dbConfig = require('./src/config/database');
const connectionDb = new Sequelize(dbConfig);

module.exports = connectionDb;